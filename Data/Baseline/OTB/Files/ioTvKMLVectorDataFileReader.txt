VectorData (0x5e3c2d00e120)
  RTTI typeinfo:   otb::VectorData<double, 2u, double>
  Reference Count: 2
  Modified Time: 280
  Debug: Off
  Object Name: 
  Observers: 
    none
  Source: (0x5e3c2d00e410) 
  Source output name: Primary
  Release Data: Off
  Data Released: False
  Global Release Data: Off
  PipelineMTime: 18
  UpdateMTime: 281
  RealTimeStamp: 0 seconds 

+Root (Root)
  +Document (Paths)
    +Line () 2 points
  -> Metadata:    VectorData Keyword list:    - Size: 2
      Name (String): Tessellated
      Description (String): If the <tessellate> tag has a value of 1, the line will contour to the underlying terrain

    +Line () 2 points
  -> Metadata:    VectorData Keyword list:    - Size: 2
      Name (String): Untessellated
      Description (String): If the <tessellate> tag has a value of 0, the line follow a simple straight-line path from point to point

    +Line () 11 points
  -> Metadata:    VectorData Keyword list:    - Size: 2
      Name (String): Absolute
      Description (String): Transparent purple line

    +Line () 11 points
  -> Metadata:    VectorData Keyword list:    - Size: 2
      Name (String): Absolute Extruded
      Description (String): Transparent green wall with yellow outlines

    +Line () 11 points
  -> Metadata:    VectorData Keyword list:    - Size: 2
      Name (String): Relative
      Description (String): Black line (10 pixels wide), height tracks terrain

    +Line () 11 points
  -> Metadata:    VectorData Keyword list:    - Size: 2
      Name (String): Relative Extruded
      Description (String): Opaque blue walls with red outline, height tracks terrain

